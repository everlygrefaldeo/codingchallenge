package com.codingcanary.codingchallenge.actvities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.codingcanary.codingchallenge.R;
import com.codingcanary.codingchallenge.utils.Other_Functions;
import com.codingcanary.codingchallenge.utils.SharedPref;
import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {
    private ImageView imageView;
    private TextView nameTxt, priceTxt, genreTxt, descTxt;
    private SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String trackprice = intent.getStringExtra("trackprice");
        String genre = intent.getStringExtra("genre");
        String imageUrl = intent.getStringExtra("imageUrl");
        String currency = intent.getStringExtra("currency");
        String desc = intent.getStringExtra("desc");
        sharedPref = SharedPref.getInstance(DetailsActivity.this);


        imageView = (ImageView) findViewById(R.id.image);
        nameTxt = (TextView) findViewById(R.id.name);
        priceTxt = (TextView) findViewById(R.id.price);
        genreTxt = (TextView) findViewById(R.id.genre);
        descTxt = (TextView) findViewById(R.id.desc);
        //Picasso for smooth loading
        Picasso.get()
                .load(imageUrl)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.error_placeholder)
                .into(imageView);


        nameTxt.setText(name);
        priceTxt.setText(currency + " " + trackprice);
        genreTxt.setText(genre);
        descTxt.setText(desc);

    }
    @Override
    protected void onStart() {
        super.onStart();
        //removes shareddpref date and time
        sharedPref.removeDateTime();
        //saves another dateTime isntance
        sharedPref.saveDateTime(Other_Functions.getCurrentTime());
    }

    @Override
    protected void onPause() {
        super.onPause();

        sharedPref.removeDateTime();
        sharedPref.saveDateTime(Other_Functions.getCurrentTime());
    }

    }

