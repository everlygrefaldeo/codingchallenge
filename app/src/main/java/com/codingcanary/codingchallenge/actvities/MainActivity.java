package com.codingcanary.codingchallenge.actvities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codingcanary.codingchallenge.R;
import com.codingcanary.codingchallenge.adapters.DetailsAdapter;
import com.codingcanary.codingchallenge.objects.Object_Details;
import com.codingcanary.codingchallenge.utils.GLOBAL_URL;
import com.codingcanary.codingchallenge.utils.InternetConnection;
import com.codingcanary.codingchallenge.utils.SharedPref;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private HttpResponse response;
    private List<Object_Details> list = new ArrayList<>();
    private DetailsAdapter adapter;
    private SharedPref sharedPref;
    private TextView lastAtiveTextView;
    private InternetConnection internetConnection;
    private Boolean isInternetPresent = false;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lastAtiveTextView = (TextView) findViewById(R.id.lastActiveTxt);
        listView = (ListView) findViewById(R.id.listView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        new GetDetailsAsyncTask().execute();

        StrictMode.ThreadPolicy
        policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPref = SharedPref.getInstance(MainActivity.this);
        lastAtiveTextView.setText("");
        //sets last time active for 5000 Millis

        if (sharedPref.getDateTime().isEmpty()) {
            lastAtiveTextView.setText("No Previous logins from this phone.");
            lastAtiveTextView.postDelayed(new Runnable() {
                public void run() {
                    lastAtiveTextView.setVisibility(View.INVISIBLE);
                }
            }, 5000);

        } else {
            lastAtiveTextView.setText(sharedPref.getDateTime());
            lastAtiveTextView.postDelayed(new Runnable() {
                public void run() {
                    lastAtiveTextView.setVisibility(View.INVISIBLE);
                }
            }, 5000);
        }
        //check internet connection
        internetConnection = new InternetConnection(MainActivity.this);
        isInternetPresent = internetConnection.isConnectingToInternet();
        if (!isInternetPresent) {

            Toast.makeText(MainActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

        private class GetDetailsAsyncTask extends AsyncTask<String, String, String> {


            JSONObject object = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
                internetConnection = new InternetConnection(MainActivity.this);
                isInternetPresent = internetConnection.isConnectingToInternet();
                if (!isInternetPresent) {

                    Toast.makeText(MainActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            protected String doInBackground(String... params) {

                internetConnection = new InternetConnection(MainActivity.this);
                isInternetPresent = internetConnection.isConnectingToInternet();
                if (!isInternetPresent) {

                    Toast.makeText(MainActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();

                }

                HttpClient httpClient = new DefaultHttpClient();
                String result = "";

                HttpGet httpGet = new HttpGet();
                List<NameValuePair> httpGetParams = new ArrayList<NameValuePair>();


                try {
                    httpGet.setURI(new URI(

                            GLOBAL_URL.ITUNES_URL + URLEncodedUtils.format(httpGetParams, "utf-8")));

                    response = httpClient.execute(httpGet);

                    InputStream inputStream = response.getEntity().getContent();

                    InputStreamReader inputStreamReader = new InputStreamReader(
                            inputStream);

                    BufferedReader bufferedReader = new BufferedReader(
                            inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    result = stringBuilder.toString();

                    System.out.println("Returning value of doInBackground :"
                            + stringBuilder.toString());

                    object = new JSONObject(result);


                } catch (Exception e) {

                }

                try {

                } catch (Exception e) {

                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                progressBar.setVisibility(View.GONE);
                internetConnection = new InternetConnection(MainActivity.this);
                isInternetPresent = internetConnection.isConnectingToInternet();
                if (!isInternetPresent) {

                    Toast.makeText(MainActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();

                }

                if (response.getStatusLine().toString().contains("20")) {
                    if (list != null) {
                        list.clear();
                    } else {
                        list = new ArrayList<Object_Details>();
                    }


                    try {

                        JSONObject jsonObject = new JSONObject(result);
                        JSONArray jsonArray = jsonObject.getJSONArray("results");
                        for (int k = 0; jsonArray.length() > k; k++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(k);
                            String name = jsonObject1.optString("trackName");
                            String genreName = jsonObject1.optString("primaryGenreName");
                            String trackPrice = jsonObject1.optString("trackPrice");
                            String urlLInk = jsonObject1.optString("artworkUrl100");
                            String currency = jsonObject1.optString("currency");
                            String description = jsonObject1.optString("longDescription");


                            //setting object
                            Object_Details details = new Object_Details();
                            details.setName(name);
                            details.setGenre(genreName);
                            details.setTrackPrice(trackPrice);
                            details.setImageUrl(urlLInk);
                            details.setCurrency(currency);
                            details.setDesc(description);
                            //  adding to list
                            list.add(details);


                            adapter = new DetailsAdapter(MainActivity.this, list);
                            listView.setAdapter(adapter);
                        }


                    } catch (JSONException e) {

                        e.printStackTrace();

                    }


                } else {
                    Toast.makeText(MainActivity.this, "An error was encountered.Please try again later.", Toast.LENGTH_SHORT).show();
                }



                }


            }
        }
