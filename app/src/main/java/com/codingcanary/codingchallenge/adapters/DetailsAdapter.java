package com.codingcanary.codingchallenge.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.codingcanary.codingchallenge.R;
import com.codingcanary.codingchallenge.actvities.DetailsActivity;
import com.codingcanary.codingchallenge.objects.Object_Details;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EverlyGrefaldeo on 8/21/2019.
 */

public class DetailsAdapter extends BaseAdapter {

    Context m_context;
    LayoutInflater inflater;
    private List<Object_Details> list = null;
    private ArrayList<Object_Details> arrayList;

    public DetailsAdapter(Context m_context, List<Object_Details> list) {
        this.m_context = m_context;
        this.list = list;
        inflater = LayoutInflater.from(m_context);
        this.arrayList = new ArrayList<Object_Details>();
        this.arrayList.addAll(list);

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Viewholder holder;
        if (view == null) {

            view = inflater.inflate(R.layout.item, null);
            holder = new Viewholder();
            holder.nameTxt = (TextView) view.findViewById(R.id.name);
            holder.trackPriceTxt= (TextView) view.findViewById(R.id.trackPrice);
            holder.genreTxt = (TextView) view.findViewById(R.id.genre);
            holder.imageView = (ImageView) view.findViewById(R.id.image);
            view.setTag(holder);

        } else {
            holder = (Viewholder) view.getTag();
        }
        final String name = list.get(i).getName();
        final String trackprice = list.get(i).getTrackPrice();
        final String genre = list.get(i).getGenre();
        final String imageUrl = list.get(i).getImageUrl();
        final String currency = list.get(i).getCurrency();
        final String desc=list.get(i).getDesc();


        holder.nameTxt.setText(name);
        holder.trackPriceTxt.setText(currency +" " +trackprice);
        holder.genreTxt.setText(genre);
        Picasso.get()
                .load(imageUrl)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.error_placeholder)
                .into(holder.imageView);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(m_context, DetailsActivity.class);
                intent.putExtra("name", name);
                intent.putExtra("trackprice", trackprice);
                intent.putExtra("genre", genre);
                intent.putExtra("imageUrl", imageUrl);
                intent.putExtra("currency",currency);
                intent.putExtra("desc",desc);
                m_context.startActivity(intent);
            }
        });

        return view;
    }

    private class Viewholder {
        TextView nameTxt, trackPriceTxt, genreTxt, desc;
        ImageView imageView;
    }
}
