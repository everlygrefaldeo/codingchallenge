package com.codingcanary.codingchallenge.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by EverlyGrefaldeo on 8/21/2019.
 */

public class Other_Functions {


    public static String getCurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }
}
