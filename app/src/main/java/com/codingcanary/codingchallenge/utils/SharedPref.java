package com.codingcanary.codingchallenge.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by EverlyGrefaldeo on 8/21/2019.
 */

public class SharedPref {

    private static SharedPref sharedPref= new SharedPref();
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static String llastDate= "";

    public SharedPref() {
    }
    public static SharedPref getInstance(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
        return sharedPref;
    }
    public String getDateTime(){
         return sharedPreferences.getString(llastDate,"");
    }
    public void removeDateTime(){
        editor.remove(llastDate);
        editor.commit();
    }
    public void logOut(){
        editor.clear();
        editor.commit();
    }
    public void saveDateTime(String date){
        editor.putString(llastDate,"Last active on: " + date);
        editor.commit();
    }

}
